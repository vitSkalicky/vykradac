# Vykradač

Import songs from popular chord websites into [bard][1].

Command-line program and rust library for scraping and parsing song lyrics and chords from some popular websites outputting them in [bard's markdown-like format][2]

Supported websites:

- https://pisnicky-akordy.cz/
- https://supermusic.cz/

## Usage

Download the binary from [releases][3], open command line and run `.\vykradac-cli.exe` (Linux: `./vykradac-cli`). Then write urls of songs separated by newline and they will be saved into folder `songs` in the current directory.

For detailed usage run the program with `--help` (like `./vykradac-cli --help`)

## Development

Written in pure rust. To build, [install rust][4] and run `cargo build` in the project's directory.

For documentation, see comments and doc comments in the code.

[1]: https://github.com/vojtechkral/bard
[2]: https://github.com/vojtechkral/bard/blob/main/doc/markdown.md
[3]: https://gitlab.com/vitSkalicky/vykradac/-/releases
[4]: https://www.rust-lang.org/tools/install