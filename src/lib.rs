use reqwest::Url;
use select::document::Document;
use select::predicate::{Attr, Class, Name, Predicate};
use std::error::Error;
use std::fmt;
use unicode_segmentation::UnicodeSegmentation;

/// Represents a song's data
#[derive(Clone)]
pub struct Song {
    pub title: String,
    pub artist: String,
    /// In bard markdown syntax
    pub body: String,
}

/// Detects website and gets the song if supported. If the website is not supported `Err(Box::new(UnsupportedWebsite{...}))` is returned.
pub async fn get_song(url: Url) -> Result<Song, Box<dyn Error>> {
    match url.host_str() {
        Some("pisnicky-akordy.cz") => pisnicky_akordy(url).await,
        Some("supermusic.cz") => supermusic(url).await,
        _ => Err(Box::new(UnsupportedWebsite {
            host: url.host_str().map(|x| x.to_string()),
        })),
    }
}

/// Downloads and parses songs from [https://pisnicky-akordy.cz/]
async fn pisnicky_akordy(url: Url) -> Result<Song, Box<dyn Error>> {
    let resp = reqwest::get(url).await?.text().await?;
    let document = Document::from(&resp[..]);

    // extract song title
    let title = document
        .find(Attr("id", "songheader").child(Name("h1")).child(Name("a")))
        .take(1)
        .next()
        .map(|x| x.text())
        .unwrap_or("???".to_string());

    // extract artist name
    let artist = document
        .find(
            Attr("id", "songheader")
                .child(Name("h2").and(Class("pull-right")))
                .child(Name("a")),
        )
        .take(1)
        .next()
        .map(|x| x.text())
        .unwrap_or("???".to_string());

    // extract song text
    let body_node = document
        .find(Attr("id", "songtext").child(Name("pre")))
        .take(1)
        .next();
    // guess which lines contain lyrics and which contain chords
    let body_lines: Vec<Line> = if let Some(body_node) = body_node {
        body_node.children().flat_map(|x|
        // chord lines are enclosed in <el> tags
        if x.name() == Some("el"){
            vec![Line::Chords(x.text().trim_end().to_string())]
        }else{
            x.text().lines().map(|ln|Line::Lyrics(ln.to_string())).collect()
        }
        ).collect()
    } else {
        vec![]
    };

    // convert from chords on line before lyrics into inlined chords
    let inlined_chords = normalize_chorus_mark(&inline_chords(body_lines));

    return Ok(Song {
        body: format!("# {}\n## {}\n\n{}", &title, &artist, inlined_chords), // also adds title and artist in the bard's markdown format to the body
        title,
        artist,
    });
}

/// Downloads and parses songs from [https://supermusic.cz/]
async fn supermusic(url: Url) -> Result<Song, Box<dyn Error>> {
    let resp = reqwest::get(url.clone()).await?.text().await?;
    let document = Document::from(&resp[..]);

    // Artist name
    let artist = document
        .find(Name("font").and(Attr("color", "#4AEF21")).child(Name("a")))
        .filter(|node| {
            node.attr("href")
                .map_or(false, |x| x.starts_with("skupina.php?idskupiny="))
        })
        .take(1)
        .next()
        .map(|x| x.text().trim().to_string())
        .unwrap_or("???".to_string());

    // extract song title
    let title = document
        .find(Class("test3"))
        .take(1)
        .next()
        .map(|x| x.text().trim().to_string())
        .unwrap_or("???".to_string())
        .replace(&format!("{} - ", artist), "");

    // extract song text - use export feature
    let body = if let Some(id_pair) = url.query_pairs().filter(|x| x.0 == "idpiesne").next() {
        let id = id_pair.1;
        let url = Url::parse(&format!(
            "https://supermusic.cz/export.php?idpiesne={id}&stiahni=1&typ=TXT"
        ))?;
        let mut resp = reqwest::get(url).await?.text().await?.replace("\r", "");
        resp.replace_range(
            0..title.len() + 3, /*remove the space before and two newlines after*/
            "",
        );
        Some(
            resp.replace("[", "`")
                .replace("]", "`")
                .replace("/:", "[:")
                .replace(":/", ":]")
                .replace("®:", "> ")
                .replace("®", "!>")
                .replace("REF:", "Ref:")
                .replace("ref:", "Ref:")
                .replace("Ref: \n", "> ")
                .replace("Ref:", ">"),
        )
    } else {
        None
    };

    // convert from chords on line before lyrics into inlined chords
    let inlined_chords = normalize_chorus_mark(&body.unwrap_or("???".to_string()));

    return Ok(Song {
        body: format!("# {}\n## {}\n\n{}", &title, &artist, inlined_chords), // also adds title and artist in the bard's markdown format to the body
        title,
        artist,
    });
}

/// shortcut
trait GraphemeLen {
    fn grapheme_len(&self) -> usize;
}

impl GraphemeLen for String {
    fn grapheme_len(&self) -> usize {
        self.graphemes(true).count()
    }
}
impl GraphemeLen for &str {
    fn grapheme_len(&self) -> usize {
        self.graphemes(true).count()
    }
}

/// Used by [inline_chords].
enum Line {
    /// Contains chords for the next line offset using spaces. Do not use any other whitespace.
    Chords(String),
    /// Normal line - nothing special
    Lyrics(String),
}

/// Takes a list of lines, each is either a normal line with lyrics ([Line::Lyrics]) or a line with chords for the next line offset with spaces ([Line::Chords]) and converts it into format with chords in backtics inlined in lyrics. See example below.
///
/// In the following example, the first, third and fifth lines should be [Line::Chords], the rest (including the blank line) should be [Line::Lyrics].
/// ```
/// E D A G
///
/// E       D     A       G
/// Holinky nosil vždycky naruby
/// E         D       A       G
/// Myslel si, že je to známka punku
/// ```
/// The example above will be converted to this:
/// ```
/// `E` `D` `A` `G`
/// `E`Holinky `D`nosil `A`vždycky `G`naruby
/// `E`Myslel si,`D` že je t`A`o známka`G` punku
/// ```
fn inline_chords(lines: Vec<Line>) -> String {
    let mut res = String::new();
    let mut last_chords: Vec<(usize, String)> = Vec::new();

    for item in lines {
        match item {
            Line::Chords(line) => {
                if !last_chords.is_empty() {
                    //two chord lines in a row - put the chords from the first one onto and otherwise empty line
                    let mut pushed = 0; //number of graphemes pushed
                    for s in &last_chords {
                        // add spaces to make the chord start at the right index
                        if pushed < s.0 {
                            res.push_str(&" ".repeat(s.0 - pushed));
                            pushed += s.0 - pushed
                        }
                        let chrd_txt = &format!("`{}` ", s.1);
                        res.push_str(chrd_txt);
                        pushed += chrd_txt.grapheme_len()
                    }
                    last_chords.clear();
                    res.push_str("\n")
                }
                let mut i = 0;
                let grph: Vec<&str> = line.graphemes(true).collect();
                while i < grph.len() {
                    if grph[i] == " " {
                        // do not use other whitespace
                        i += 1;
                        continue;
                    }
                    let start_i = i;
                    let mut chrd = String::new();
                    while i < grph.len() && grph[i] != " " {
                        chrd.push_str(grph[i]);
                        i += 1;
                    }
                    last_chords.push((start_i, chrd))
                }
            }
            Line::Lyrics(line) => {
                let mut line_res = String::new();
                let mut last = 0;
                let mut grph: Vec<&str> = line.graphemes(true).collect();

                let last_chord = last_chords.last().map_or(0, |x| x.0);
                if last_chord > grph.len() {
                    for _ in grph.len()..last_chord {
                        grph.push(" ");
                    }
                }
                for cp in &last_chords {
                    line_res.push_str(&grph[last..cp.0].join(""));
                    if !cp.1.trim().is_empty() {
                        line_res.push_str(&format!("`{}`", cp.1))
                    }
                    last = cp.0
                }
                line_res.push_str(&grph[last..].join(""));
                last_chords.clear();
                line_res.push('\n');

                res.push_str(&line_res)
            }
        }
    }
    return res;
}

/// Normalizes how chorus is marked
fn normalize_chorus_mark(text: &str) -> String {
    let mut res = String::new();
    for line in text.lines() {
        let line = String::from(line);
        let line = if line.starts_with("R:") {
            line.replacen("R:", ">", 1)
        } else {
            line
        };
        let line = if line.trim_end().ends_with(">") {
            line.trim_end().replacen(">", "!>", 1)
        } else {
            line
        };
        res.push_str(&line);
        res.push_str("\n");
    }
    return res;
}

/// Custom error type
#[derive(Debug)]
pub struct UnsupportedWebsite {
    host: Option<String>,
}

impl std::error::Error for UnsupportedWebsite {}

impl fmt::Display for UnsupportedWebsite {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(host) = &self.host {
            write!(f, "Website '{}' is not supported :(", host)
        } else {
            write!(f, "Malformed URL address") //technically not true, but hey
        }
    }
}
