use regex::Regex;
use reqwest::Url;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use vykradac::get_song;
use clap::Parser;

/// CLI Args
#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    /// File extension of the output files
    #[arg(short,long, value_name="EXTENSION", default_value = "md")]
    file_extension: String,

    /// Where to save songs
    #[arg(short, long, value_name="DIR", default_value = "songs")]
    out_dir: PathBuf,

    /// Do not save to file, but only print to standart output. Status messages will not be printed if enabled.
    #[arg(short, long)]
    print_only: bool
}

/// CLI for the library
#[tokio::main]
async fn main() {
    let args = Args::parse();

    let songspath = args.out_dir;
    fs::create_dir(&songspath).unwrap_or_default();

    let extension = if args.file_extension.trim().is_empty() { String::from("") } else {format!(".{}", args.file_extension)};

    let mut eof = false;
    let mut buf = String::new();
    while !eof {
        buf.clear();
        // if read_line returns Ok(0), the EOF is reached
        eof = 0
            == io::stdin()
                .read_line(&mut buf)
                .expect("Cannot read standard input");
        let url = Url::parse(&buf);
        if let Ok(url) = url {
            // use function of the lib to get the songs data
            let song = get_song(url).await;
            match song {
                Ok(song) => {
                    if args.print_only {
                        println!("{}", song.body);
                    }
                    let filename = String::from(&song.artist) + " - " + &song.title + &extension[..];
                    let re = Regex::new(r#"[\\/:*?"<>|]"#).unwrap(); // forbidden problematic characters - not 100% safe, but good enough
                    let filename: String = re.replace_all(&filename, "").to_string();
                    let file_path = songspath.join(Path::new(&filename));

                    // write song to file
                    if !args.print_only {
                        match fs::write(file_path, song.body) {
                            Ok(()) => {
                                println!("OK: {}", filename);
                            }
                            Err(err) => {
                                println!("ERROR: {}", err);
                            }
                        }
                    }
                }
                Err(err) => {
                    if !args.print_only { println!("ERROR: {}", err); }
                }
            }
        } else {
            if !args.print_only { println!("ERROR: Not a valid URL address"); }
        }
    }
}

