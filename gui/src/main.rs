#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

use eframe::egui::{self};
use egui::{Color32, RichText, TextEdit};
use poll_promise::Promise;
use reqwest::Url;
use vykradac::Song;

fn main() {
    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(320.0, 240.0)),
        ..Default::default()
    };
    eframe::run_native(
        "Vykradač",
        options,
        Box::new(|_cc| Box::new(MyApp::default())),
    );
}

struct MyApp {
    url: String,
    download_promise: Option<Promise<Result<Song, String>>>,
    song: Option<Song>,
    err_text: Option<String>,
}

impl Default for MyApp {
    fn default() -> Self {
        Self {
            url: "".to_string(),
            download_promise: None,
            song: None,
            err_text: None,
        }
    }
}

impl MyApp {
    fn import(url: &str) -> Promise<Result<Song, String>> {
        let url_str = String::from(url);
        Promise::spawn_thread("import song", move || {
            let rt = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap();
            match Url::parse(&url_str) {
                Ok(url) => match rt.block_on(vykradac::get_song(url)) {
                    Ok(song) => Ok(song),
                    Err(e) => Err(e.to_string()),
                },
                Err(e) => Err(format!("Špatná URL adresa: {}", e)),
            }
        })
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| { egui::ScrollArea::vertical().show(ui, |ui|{
            if let Some(promise) = self.download_promise.take() {
                ui.label("Načítání...");
                ui.spinner();
                match promise.try_take() {
                    Ok(Ok(song)) => {self.download_promise = None; self.song = Some(song); self.err_text = None},
                    Ok(Err(err)) => {self.download_promise = None; self.song = None;self.err_text = Some(err)}
                    Err(prom) => {self.download_promise = Some(prom)}
                }
            }else{
                ui.heading("Importovat písničku");
                ui.label("Funkční stránky:");
                ui.hyperlink("https://supermusic.cz/");
                ui.horizontal_wrapped(|ui| {
                    ui.hyperlink("https://pisnicky-akordy.cz/");
                    ui.label("(Pokud chceš písničku od jiného akordu, zvol jej nejprve na pisnicky-akordy.cz, pak klikni na šipku vedle ozubeného kolečka, klikni na tisk, vyskočí okno s verzí pro tisk a od vybraného akordu a zkopíruj URL z tohoto okna)");
                });
                ui.add_space(5.0);
                ui.horizontal(|ui| {
                    ui.label("Vlož URL adresu písničky:");
                    ui.add(TextEdit::singleline(&mut self.url));
                });
                if ui.button("importovat").clicked() {
                    self.download_promise = Some(MyApp::import(&self.url[..]));
                }

                // show error text is the is some
                if let Some(ref err_text) = self.err_text {
                    ui.label(RichText::new(err_text).color(Color32::RED));
                }

                if let Some(song) = &mut self.song {
                    ui.add_space(5.0);
                    ui.label("zkopíruj text níže");
                    // text edit
                    ui.add(
                            egui::TextEdit::multiline(&mut song.body)
                        .font(egui::TextStyle::Monospace) // for cursor height
                        .code_editor()
                        //.desired_rows(10)
                        .lock_focus(true)
                        .desired_width(f32::INFINITY)
                        //.layouter(&mut layouter),
                    );
                }
            }
        })});
    }
}
